import { Company } from '../../company/entities/company.entity';
import { Address } from '../../address/entities/address.entity';
import { Merchant } from '../../merchant/entities/merchant.entity';

export class Invoice {
  id: number;
  company?: Company | null;
  companyId: number | null;
  billingAddress?: Address | null;
  billingAddressId: number | null;
  merchant?: Merchant | null;
  merchantId: number | null;
  number: string;
  po_number: string;
  currency: string;
  amount: number;
}
