export class UpdateInvoiceDto {
  number?: string;
  po_number?: string;
  currency?: string;
  amount?: number;
}
