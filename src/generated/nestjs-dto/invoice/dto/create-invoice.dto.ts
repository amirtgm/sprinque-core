export class CreateInvoiceDto {
  number: string;
  po_number: string;
  currency: string;
  amount: number;
}
