
import {Company} from '../../company/entities/company.entity'
import {Invoice} from '../../invoice/entities/invoice.entity'


export class Merchant {
  id: number ;
name: string ;
company?: Company ;
companyId: number ;
invoices?: Invoice[] ;
}
