
import {Address} from '../../address/entities/address.entity'
import {Invoice} from '../../invoice/entities/invoice.entity'
import {Merchant} from '../../merchant/entities/merchant.entity'


export class Company {
  id: number ;
email: string ;
name: string ;
password: string ;
address?: Address  | null;
addressId: number  | null;
Invoices?: Invoice[] ;
Merchant?: Merchant[] ;
}
