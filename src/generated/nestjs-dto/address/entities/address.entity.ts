import { Company } from '../../company/entities/company.entity';
import { Invoice } from '../../invoice/entities/invoice.entity';

export class Address {
  id: number;
  firstname: string | null;
  lastname: string | null;
  address: string;
  line1: string;
  line2: string | null;
  city: string;
  state: string | null;
  zipcode: string | null;
  country: string;
  phone_number: string | null;
  email: string | null;
  company?: Company | null;
  invoice?: Invoice | null;
}
