export class UpdateAddressDto {
  firstname?: string;
  lastname?: string;
  address?: string;
  line1?: string;
  line2?: string;
  city?: string;
  state?: string;
  zipcode?: string;
  country?: string;
  phone_number?: string;
  email?: string;
}
