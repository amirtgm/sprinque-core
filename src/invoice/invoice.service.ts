import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class InvoiceService {
  constructor(private readonly prisma: PrismaService) {}
  create(createInvoiceDto: Prisma.InvoiceCreateInput) {
    return this.prisma.invoice.create({ data: createInvoiceDto });
  }

  findAll() {
    return this.prisma.invoice.findMany();
  }

  findOne(id: number) {
    return this.prisma.invoice.findUnique({
      where: {
        id,
      },
    });
  }

  update(id: number, updateInvoiceDto: Prisma.InvoiceUpdateInput) {
    return this.prisma.invoice.update({
      where: {
        id,
      },
      data: updateInvoiceDto,
    });
  }

  remove(id: number) {
    return this.prisma.invoice.delete({
      where: {
        id,
      },
    });
  }
}
