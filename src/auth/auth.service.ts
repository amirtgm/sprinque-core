import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Company } from '@prisma/client';
import { CompanyService } from 'src/company/company.service';

@Injectable()
export class AuthService {
  constructor(
    private companyService: CompanyService,
    private jwtService: JwtService,
  ) {}

  async validateToken(id: number): Promise<Company | null> {
    const company = await this.companyService.findOne(id);
    if (company) {
      return company;
    }
    return null;
  }
  async createToken(id): Promise<{ token: string }> {
    const company = await this.validateToken(id);
    if (company) {
      const token = this.jwtService.sign({ id: company.id });
      return {
        token,
      };
    }
    return null;
  }
}
