import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { CompanyService } from 'src/company/company.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants.auth';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PrismaModule,
    JwtModule.register({
      secret: jwtConstants.secret,

      signOptions: { expiresIn: '1000 d' },
    }),
  ],
  providers: [AuthService, CompanyService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
