import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { CompanyService } from './company.service';
import { AuthService } from 'src/auth/auth.service';
import { JwtAuthGuard } from 'src/auth/auth.jwt.guard';
import { CreateCompanyDto } from 'src/generated/nestjs-dto/company/dto/create-company.dto';
import { UpdateCompanyDto } from 'src/generated/nestjs-dto/company/dto/update-company.dto';
// import {  } from 'class-transformer';
@Controller('company')
export class CompanyController {
  constructor(
    private readonly companyService: CompanyService,
    private authService: AuthService,
  ) {}

  @Post()
  create(@Body() createCompanyDto: CreateCompanyDto) {
    return this.companyService.create(createCompanyDto);
  }

  @Post('generate-api-key')
  generateApiKey(@Query('id', new ParseIntPipe()) id: string) {
    return this.authService.createToken(id);
  }

  // @Get()
  // findAll() {
  //   return this.companyService.findAll();
  // }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.companyService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCompanyDto: UpdateCompanyDto) {
    return this.companyService.update(+id, updateCompanyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.companyService.remove(+id);
  }
}
