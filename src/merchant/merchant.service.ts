import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class MerchantService {
  constructor(private readonly prisma: PrismaService) {}
  create(createMerchantDto: Prisma.MerchantCreateInput) {
    return this.prisma.merchant.create({ data: createMerchantDto });
  }

  findAll() {
    return this.prisma.merchant.findMany();
  }

  findOne(id: number) {
    return this.prisma.merchant.findUnique({
      where: {
        id,
      },
    });
  }

  update(id: number, updateMerchantDto: Prisma.MerchantUpdateInput) {
    return this.prisma.merchant.update({
      where: {
        id,
      },
      data: updateMerchantDto,
    });
  }

  remove(id: number) {
    return this.prisma.merchant.delete({
      where: {
        id,
      },
    });
  }
}
